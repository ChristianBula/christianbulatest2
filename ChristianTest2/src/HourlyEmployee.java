public class HourlyEmployee implements Employee{
	//initizialising variables
     private double hoursWorked;
     private double hourlyPay;
     
     public HourlyEmployee(double hoursWorked, double hourlyPay) {
    	 this.hoursWorked = hoursWorked;
    	 this.hourlyPay = hourlyPay;
     }
     //this method calculates the yearly pay of the employee
     public double getYearlyPay() {
    	 return this.hoursWorked*this.hourlyPay*52;
     }
}

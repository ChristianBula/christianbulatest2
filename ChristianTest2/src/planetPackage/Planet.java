package planetPackage;

import java.util.Objects;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	//overriding equals for it to only return true if the name and the order is equal
	public boolean equals(Planet b) {
		if(this.name == b.getName() && this.order == b.getOrder()) {
			return true;
		}
		else {
			return false;
		}
	}
	public int hashCode() {
		return Objects.hash(this.getName(),this.getOrder());
	}
	public int compareTo(Object y) {
		Planet x = (Planet)y;
		int compareReturn;
		compareReturn = this.getName().compareTo(x.getName());
		if(compareReturn == 0) {
			compareReturn = this.getPlanetarySystemName().compareTo(x.getPlanetarySystemName());
			if(compareReturn == -1) {
				compareReturn = 1;
				return compareReturn;
			}
			else if(compareReturn == 1) {
				compareReturn = -1;
				return compareReturn;
			}
		}
		else {
			return compareReturn;
		}
		return compareReturn;
	}
}

package planetPackage;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionMethods {
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		//creating new collection to store the planets that satisfy the instruction;
        Collection<Planet> newPlanets = new ArrayList<Planet>();
		
		for(Planet elem : planets) {
			if(elem.getOrder() == 1 || elem.getOrder() == 2 || elem.getOrder() == 3) {
				newPlanets.add(elem);
			}
		}
		
		return newPlanets;
		
	}
}


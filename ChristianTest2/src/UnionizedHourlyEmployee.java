
public class UnionizedHourlyEmployee extends HourlyEmployee{
	
	private double pensionContribution;
	
	//since this class extends hourlyEmployee, I just use super to get the hourlyPay and hoursWorked
	public UnionizedHourlyEmployee(double hoursWorked, double hourlyPay,double pensionContribution) {
		super(hoursWorked, hourlyPay);
		this.pensionContribution = pensionContribution;
	}
    public double getYearlyPay() {
    	//using super to get the yearlyPay of the employee and adding the pension afterwards
    	return super.getYearlyPay() + this.pensionContribution;
    }
}

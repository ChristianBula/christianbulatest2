//this interface is implemented by all the different employees. They all need a getYearlyPay();
public interface Employee {
    double getYearlyPay();
}

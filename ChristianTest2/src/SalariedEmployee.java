public class SalariedEmployee implements Employee{
	private double yearlySalary;
	
	public SalariedEmployee(double salary){
		this.yearlySalary = salary;
	}
	//this method returns the yearlySalary
    public double getYearlyPay() {
        return this.yearlySalary;
    }
}

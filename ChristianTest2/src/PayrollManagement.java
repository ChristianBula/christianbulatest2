
public class PayrollManagement {
	
	public static void main(String[] args) {
		//Creating employee array and the different employee objects
		Employee[] employees = new Employee[5];
		HourlyEmployee h = new HourlyEmployee(10,20);
		UnionizedHourlyEmployee u = new UnionizedHourlyEmployee(40,18,5000);
		UnionizedHourlyEmployee u2 = new UnionizedHourlyEmployee(35,20,2000);
		SalariedEmployee s = new SalariedEmployee(40000);
		SalariedEmployee s2 = new SalariedEmployee(10000);
		
		//adding the objects to the array
		employees[0] = h;
 		employees[1] = u;
		employees[2] = u2;
		employees[3] = s;
		employees[4] = s2;
		//running the getTotalExpenses method
		System.out.println(getTotalExpenses(employees));
	}
	
				
   public static double getTotalExpenses(Employee[] e) {
	   double totalExpenses = 0;
	   //this loop will take each yearlyPay and add it together
	   for (Employee elem : e) {
	     totalExpenses += elem.getYearlyPay();
    }
	   return totalExpenses;
  }
}